!function ($) {

	"use strict";
	var FOUNDATION_VERSION = '6.3.1';

	// Owl slider Declarations
	$('.hero-carousel').each(function() {
		var $this = $(this);
		$this.owlCarousel({
			items: 1,
			loop:true,
			margin: 0,
			autoplay:true,
			autoplayTimeout:7000,
			autoplayHoverPause:true,
			stopOnHover: true,
			lazyLoad: true,
			lazyEffect: "fade",
			smartSpeed: 800, // duration of change of 1 slide
			dots: true,
			nav:true,
			navClass: ['owl-prev','owl-next'],
			navText: ['<i class="fa fa-arrow-left"></i>','<i class="fa fa-arrow-right"></i>'],
			onInitialized  : counter, //When the plugin has initialized.
			onTranslated : counter //When the translation of the stage has finished.
		});
	});


function counter(event) {
   var element   = event.target;         // DOM element, in this example .owl-carousel
    var items     = event.item.count;     // Number of items
    var item      = event.item.index + 1;     // Position of the current item

  // it loop is true then reset counter from 1
  if(item > items) {
    item = item - items
  }
  $('.counter-current').html(("0" + item).slice(-2));
  $('.counter-total').html(("0" + items).slice(-2));
}




	/*
	*  new_map
	*
	*  This function will render a Google Map onto the selected jQuery element
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$el (jQuery element)
	*  @return	n/a
	*/

	function new_map( $el ) {

		// var
		var $markers = $el.find('.marker');


		// vars
		var args = {
			zoom				: 16,
			center				: new google.maps.LatLng(0, 0),
			mapTypeId			: google.maps.MapTypeId.ROADMAP,
			scrollwheel			: false,
			disableDefaultUI	: true,
		};


		// create map
		var map = new google.maps.Map( $el[0], args);


		// add a markers reference
		map.markers = [];


		// add markers
		$markers.each(function(){

	    	add_marker( $(this), map );

		});


		// center map
		center_map( map );


		// return
		return map;

	}

	/*
	*  add_marker
	*
	*  This function will add a marker to the selected Google Map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	$marker (jQuery element)
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {

				infowindow.open( map, marker );

			});
		}

	}

	/*
	*  center_map
	*
	*  This function will center the map, showing all markers attached to this map
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	4.3.0
	*
	*  @param	map (Google Map object)
	*  @return	n/a
	*/

	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){

			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

			bounds.extend( latlng );

		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}

	}

	/*
	*  document ready
	*
	*  This function will render each map when the document is ready (page has loaded)
	*
	*  @type	function
	*  @date	8/11/2013
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	// global var
	var map = null;

	$(document).ready(function(){

		$('.acf-map').each(function(){
			// create map
			map = new_map( $(this) );
		});

	// mixitup
		$(".mix-container").each(function() {
			var dppp = $(this).attr('posts-per-page');
			var mixload = $(this).attr('load');
			var inputSearch = document.querySelector('[data-ref="input-search"]');
			var keyupTimeout;

			var mixer = mixitup(this, {
				multifilter: {
					enable: true, // enable the multifilter extension for the mixer
				},
				controls: {
					// toggleDefault: 'all',
				},
				selectors: {
					control: '[data-mixitup-control]',
				},
				pagination: {
					limit: dppp,
					loop: true,
					maintainActivePage: false,
					hidePageListIfSinglePage: true,
				},
				templates: {
						pager: '<button type="button" class="${classNames}" data-page="${pageNumber}" data-mixitup-control>${pageNumber}</button>',
						pagerPrev: '<button type="button" class="${classNames}" data-page="prev" data-mixitup-control>&laquo;</button>',
						pagerNext: '<button type="button" class="${classNames}" data-page="next" data-mixitup-control>&raquo;</button>',
				},
				load: {
					filter: mixload,
				},
				callbacks: {
					onMixStart: function(state, futureState) {
						var loaditem = futureState.activeFilter.selector;
						console.log(loaditem);
					},
					onMixClick: function() {
						// Reset the search if a filter is clicked
						if (this.matches('[data-filter]')) {
								inputSearch.value = '';
						}
					},
				},
			});

			if($('.filter-group').hasClass('search-filter')) {

				inputSearch.addEventListener('keyup', function() {
						var searchValue;
						if (inputSearch.value.length < 2) {
								// If the input value is less than 3 characters, don't send
								searchValue = '';
						} else {
								searchValue = inputSearch.value.toLowerCase().trim();
						}

						// Very basic throttling to prevent mixer thrashing. Only search
						// once 350ms has passed since the last keyup event
						clearTimeout(keyupTimeout);
						keyupTimeout = setTimeout(function() {
								filterByString(searchValue);
						}, 250);
				});

				function filterByString(searchValue) {
					if (searchValue) {
						mixer.filter('[data-mixitupref*="' + searchValue + '"]');
						// mixer.filter('.type-events h2').html(searchValue);
					} else {
						mixer.filter('all');
					}
				}
				// function filterByString(archive) {
				// 	if (archive) {
				// 		mixer.filter('[data-mixitupref*="' + archive + '"]');
				// 	} else {
				// 		mixer.filter('all');
				// 	}
				// }
			}
		});

		$('a.control').click(function(e){
				e.preventDefault();
		});

	});


}(jQuery);
