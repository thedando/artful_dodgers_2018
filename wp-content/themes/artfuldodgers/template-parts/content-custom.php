<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry post-item mix'); ?> data-order="<?php echo $i; ?>">
	<header>
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<?php foundationpress_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<?php // the_content( __( 'Continue reading...', 'foundationpress' ) ); ?>
		<?php if(has_excerpt()) { the_excerpt(); } else {
				echo wp_trim_words(strip_shortcodes(get_the_content()), 15);
		} ?>
	</div>
	<?php $tag = get_the_tags(); if ( $tag ) { ?>
		<footer>
			<p><?php the_tags(); ?></p>
		</footer>
	<?php } ?>
</div>
