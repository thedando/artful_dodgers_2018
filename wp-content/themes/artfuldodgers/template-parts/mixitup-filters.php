<?php
/**
 * CUSTOM: mixitup filters
 *
 * @version: 1.2
 */

 $post_id = get_the_ID();
 if ( get_sub_field('dcf_post_listing_type') ) { $posttype = get_sub_field('dcf_post_listing_type'); } else { $posttype = get_post_type(); }
?>

  <div class="mixitup-filters <?php echo $posttype.' '; ?>">

    <?php

      $taxonomies = get_object_taxonomies($posttype, 'object');
      $count = 0;

      foreach($taxonomies as $tax) {
        if($tax->name !== 'post_format') { $count++; }
      }
      if($posttype = 'events') {
        $taxcount = $count + 2;
      } else {
        $taxcount = $count;
      }

      $count2 = 0;

      echo '<form fieldsets="'.$taxcount.'">';

        if(isset($taxonomies) && !empty($taxonomies)) {

          foreach($taxonomies as $tax) { // foreach term

            // get each taxonomy for selected post type
            $categories = get_terms( $tax->name );
            $count2++;

            if($tax->name == "post_tag") {
              $taxname = "tag";
            } else {
              $taxname = $tax->name;
            }

            if($taxname !== 'post_format') {

              if(isset($categories) && !empty($categories)) {

                echo '<fieldset class="filter-group '.$taxname.'" data-filter-group data-logic="and" taxcount="'.$count2.'">';

                  echo '<label class="label">'.$tax->label.'</label>';

                    echo '<select class="select-category">';
                      echo '<option value="">All</option>';
                      foreach($categories as $cat) {
                        if ($cat->count > 0){
                          // if($posttype = 'post') {
                          //   $cat_str = $cat->slug;
                          // } else {
                            $cat_str = $taxname.'-'.$cat->slug;
                          // }
                          echo '<option data-filter=".'.$cat_str.'" data-mixitup-control value=".'.$cat_str.'">' .$cat->name. '</option>';
                        }
                      } // end foreach

                    echo '</select>';

                echo '</fieldset>';

              } // endi if categories exists

            } // if not format

          } // end foreach term


        } // end if taxonomies exist

        if($posttype == 'events') {

          echo '<fieldset class="filter-group archive" data-filter-group data-logic="and" taxcount="'.$count2.'">';
            echo '<label class="label">Current/past</label>';
            echo '<select class="select-category">';
              echo '<option value="">All</option>';
              echo '<option data-filter=".current" data-mixitup-control value=".current">Current</option>';
              echo '<option data-filter=".past-events" data-mixitup-control value=".past-events">Past</option>';
            echo '</select>';
          echo '</fieldset>';

          echo '<fieldset class="filter-group search-filter" data-filter-group>';
            echo '<label class="label">Search</label>';
            echo '<input type="text" data-ref="input-search" data-search-attribute="data-mixitupref" data-filter="" placeholder="Search..." />';
          echo '</fieldset>';

        }

      echo '</form>';

    ?>

  </div>
