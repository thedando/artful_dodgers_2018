<?php

	// Include required module variables
		include(locate_template('template-parts/custom/VARS/header.php'));

	// Extra class for panel content
		$content_class = 'header-slider';

	// Custom Content variables
		$default_post_type = 'sliders';
		$default_post_count = 9;
		$default_order = 'ASC'; // 'DESC';
		$default_orderby = 'menu_order'; // 'date';

		// print_r($post_term_restriction);

	// WP_Query arguments
		

	// The Query 

$slides = get_field('dcf_header_hero_slider');
?>

<?php if( $slides ): ?>
	<div aria-label="Hero Slider" class="" role="region" data-count="<?php echo $count; ?>" class="<?php echo $content_class; ?>">

		<ul class="hero-carousel">

			<?php foreach( $slides as $slide): // variable must be called $post (IMPORTANT) ?>
        	<?php setup_postdata($slide); ?>

				<?php
					// ACF content fields
					$image = get_field('dcf_slide_image', $slide->ID );
					$overlay = get_field('dcf_slide_overlay', $slide->ID );
					$title = get_field('dcf_slide_title', $slide->ID );
					$caption = get_field('dcf_slide_caption', $slide->ID );
					$link_type = get_field('dcf_slide_link', $slide->ID );
					$slide_link_url = get_field('dcf_slide_link_url', $slide->ID );
					$link_text = get_field('dcf_slide_link_text', $slide->ID );
					// design options
					$float = get_field('dcf_slide_float_content', $slide->ID );
					$text_colour = get_field('dcf_slide_text_colour', $slide->ID );
					$content_bg = get_field('dcf_slide_add_background_box', $slide->ID );
					$center_align = get_field('dcf_slide_center_align_content', $slide->ID );

			

					if( !empty($image) ) {

						// Image vars
						$image_id = $image['id'];
						$image_url = $image['url'];

						// Get WP responsive markup
						$responsive_image = wp_get_attachment_image( $image_id, 'full', false, array( 'class' => 'orbit-image' ) );
						$responsive_image_src = wp_get_attachment_image_url( $image_id, 'full' );
					}

					// Increment count for active class
					$i++;
				?>

				<li class="slide<?php if ( isset($active) ) { echo $active; } ?> <?php if($overlay) { echo "overlay"; } ?>">
						<div class="slide-counter">
										<div class="valign">
											<div class="counter-current"></div>
											<div class="counter-total"></div>
										</div> 
									</div> 
					<?php if ( isset($link_type) && $link_type == 'slide' && !empty($slide_link_url['url']) ) { ?><a href="<?php echo $link_url; ?>"><?php } ?>

					<?php // if ( isset($responsive_image) ) { echo apply_filters( 'the_content', $responsive_image ); } ?>
					<?php if ( isset($caption) ) { ?>
						<figcaption class="caption <?php echo $float; ?>">
							<span class="valign">
								<div class="content <?php if($center_align) { echo "center"; } ?> <?php if($content_bg) { echo "add-bg"; } else { if(isset($text_colour)) { echo $text_colour; } } ?>">
									
									<div class="inner-content">

										<h2><?php echo $title; ?></h2>
										<p><?php echo $caption; ?></p>
										<?php
											if($content_box) {
												$white = "";
												$btn_col = "";
											}
										?>

										<?php if ( isset($link_type) && $link_type == 'button' ) { ?>
											<?php if( have_rows('dcf_slide_multiple_links') ): $button_count = 0; ?>

												<ul class="<?php //if ( isset($link_enable) && ( $split != 'split') ) { echo "header-right"; } ?> links inlinelist">
													<?php while( have_rows('dcf_slide_multiple_links') ): the_row(); $button_count++;

														$button_type = get_sub_field('button_type');

														if($content_bg) {
															if($button_count == 1) { $btn_col = "blue"; }
															else { $btn_col = ""; }
														} else {$btn_col = "white"; }

														if($button_type == "quote") {
															echo '<li class="link quote" button-no="'.$button_count.'"><a class="readmore button '.$btn_col.'" data-toggle="quote-form">Book a Service/Quote</a></li>';
														} elseif($button_type == "callback") {
															echo '<li class="link callback" button-no="'.$button_count.'"><a class="readmore button '.$btn_col.'" data-toggle="callback">Request a Callback</a></li>';
														} elseif($button_type == "custom") {
															$link_url = get_sub_field('link_url');
														?>
														<?php if( ( $link_url['url'] != '' ) && ( $link_url['title'] != '' ) ): ?>
															<li class="link custom" button-no="<?php echo $button_count; ?>">
																	<a href="<?php echo $link_url['url']; ?>" target="<?php echo $link_url['target']; ?>" class="readmore <?php echo $link_class; ?> <?php echo $white; ?>">
																		<?php echo $link_url['title']; ?>
																	</a>
															</li>
														<?php endif; ?>
														<?php } ?>
													<?php endwhile; ?>
												</ul>

											<?php endif; ?>
										<?php } ?>

									</div>

								</div>
							</span>
						</figcaption>
					<?php } ?>

					<?php if ( isset($responsive_image_src) ) { echo '<span class="bgimg" style="background-image: url(\''.$responsive_image_src.'\');"></span>'; } ?>

					<?php if ( isset($link_type) && $link_type == 'slide' && !empty($slide_link_url['url']) ) { ?></a><?php } ?>

				</li>
			<?php endforeach; ?>
		</ul>

	</div>
<?php endif; ?>

<?php
	// Restore original Post Data
	wp_reset_postdata();
?>