<?php

	// Include required module variables
		include(locate_template('template-parts/custom/VARS/modules.php'));

	// Content Module
		$module_label = "Image Gallery";
		$module_name = get_row_layout();

	// Add to default post class array
		// $post_class_array[] = 'posts-panel';

	// Extra class for panel content
		$content_class = 'content';

	// Override default module label with custom text
		if ( isset($module_title) && ( !empty($module_title) ) ) { $module_label = $module_title; }

		// Custom Content variables
			if ( get_sub_field('dcf_gridslider_items') ) { $extra_media_gallery = get_sub_field('dcf_gridslider_items'); }
			if ( get_sub_field('dcf_layout_style') ) { $layout_option = get_sub_field('dcf_layout_style'); }

?>

<?php if ( have_posts() && !$disable ) { ?>

	<article aria-label="<?php echo $module_label; ?>" data-module="<?php echo $module_name; ?>" <?php post_class($post_class_array); ?> <?php if ( isset($module_design_style) ) { echo $module_design_style; } ?>>

		<?php get_template_part( 'template-parts/custom/module/module', 'header' );  ?>

		<div class="panel-content">
			<section class="section <?php echo $content_class; ?>">

				<?php if ( isset($extra_media_gallery) && ( !empty($extra_media_gallery) ) ) { ?>
					<div class="extra-media grid-gallery">

						<?php if( $extra_media_gallery ):
							$count = count( $extra_media_gallery );
							$lastRowCount = ($count % 4);
							$lastRow = $count - $lastRowCount;
							$i = $j = 0; $l = 1;
							$lo = 0;
							$divOpen = array(0);
							$divClose = array(3);
							$lastclass = '';
						?>
							<article aria-label="Image Slider" role="region" data-count="<?php echo $count; ?>" data-orbit class="content-slider orbit">

								<ul class="orbit-container <?php if($layout_option == 'grid') { echo "grid"; } ?>">


									<?php foreach( $extra_media_gallery as $image ): ?>

										<?php

											if($layout_option == 'grid') {

												$j ++;
												if ($j == $lastRow) { $lastclass = ' only-' . $lastRowCount; }
												if (in_array($i, $divOpen)) { echo '<li class="orbit-slide slide-' . $l . $lastclass.' ">'; }

											} else {

												$lo++;
												echo '<li class="orbit-slide slide-'.$lo.'">';
											}

										?>


											<?php
												if( !empty($image) ) {

													// Image vars
													$image_id = $image['id'];
													$image_url = $image['url'];
													$caption = $image['caption'];

													// Get WP responsive markup
													$responsive_image = wp_get_attachment_image( $image_id, 'full', false, array( 'class' => 'orbit-image' ) );
													$responsive_image_src = wp_get_attachment_image_url( $image_id, 'full' );
												}
											?>
											<span class="bgimg" <?php if ( isset($responsive_image_src) ) { echo 'style="background-image: url(\''.$responsive_image_src.'\')"'; } ?>>
												<?php if ( isset($responsive_image) ) { echo apply_filters( 'the_content', $responsive_image ); } ?>
												<?php if ( isset($caption) && $caption != '' ) { ?>
													<figcaption class="orbit-caption">
														<p><?php echo $caption; ?></p>
													</figcaption>
												<?php } ?>
											</span>

										<?php

											if($layout_option == 'grid') {

												if (in_array($i, $divClose)) { echo '</li>'; $l++; }
												if ($i < 3) { $i++; $loopStamp = 1; } else { $i = ($loopStamp)? 0 : 0; }

											} else {

												echo '</li>';

											}

										?>

									<?php endforeach; ?>
								</ul>

								<?php if ( $count > 1 ) { ?>
									<div class="orbit-buttons">
										<button class="orbit-previous">
											<span class="show-for-sr">Previous Slide</span>
											<span class="nav fa fa-chevron-left"></span>
										</button>
										<button class="orbit-next">
											<span class="show-for-sr">Next Slide</span>
											<span class="nav fa fa-chevron-right"></span>
										</button>
									</div>
								<?php } ?>

							</article>

						<?php endif; ?>
					</div>
				<?php } ?>

			</section>
		</div>

	</article>

<?php } ?>

<?php
	// Restore original Post Data
	wp_reset_postdata();
?>
