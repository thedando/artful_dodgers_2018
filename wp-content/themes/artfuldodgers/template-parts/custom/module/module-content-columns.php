<?php

	// Include required module variables
		include(locate_template('template-parts/custom/VARS/modules.php'));

	// Content Module
		$module_label = "Content Columns";
		$module_name = get_row_layout();

	// Add to default post class array
		// $post_class_array[] = 'posts-panel';

	// Extra class for panel content
		$content_class = 'content-columns';

	// Override default module label with custom text
		if ( isset($module_title) && ( !empty($module_title) ) ) { $module_label = $module_title; }

	// Custom Content variables
		if ( get_sub_field('dcm_content_columns') ) { $content_columns = get_sub_field('dcm_content_columns'); }
		if ( get_sub_field('dcm_add_arrows') ) { $arrows = get_sub_field('dcm_add_arrows'); }
		// NB: No fallback

?>

<?php if ( have_posts() && !$disable ) { ?>

	<article aria-label="<?php echo $module_label; ?>" data-module="<?php echo $module_name; ?>" <?php post_class($post_class_array); ?> <?php if ( isset($module_design_style) ) { echo $module_design_style; } ?>>

		<?php get_template_part( 'template-parts/custom/module/module', 'header' );  ?>

		<div class="panel-content">

			<?php 
				$center = get_sub_field('dcf_center_align_content'); 
				$card_columns = get_sub_field('style_columns_as_cards');
				?>

			<section class="section <?php echo $content_class; ?>">

				<?php if( isset($content_columns) && !empty($content_columns) ) { $count = count($content_columns); ?>

					<?php if( have_rows('dcm_content_columns') ) : ?>

						<?php while( have_rows('dcm_content_columns') ) : the_row() ?>
							<?php
									
									$image = get_sub_field('image');
									$title = get_sub_field('title');
									$content = get_sub_field('content');
									$circle = get_sub_field('circular_image');
									$card_button = get_sub_field('card_content_button');
									$link = get_sub_field('add_a_link');
							?>
							<div class="col-<?php echo $count; ?> <?php if($center) { echo 'center'; } ?>  content-column" data-arrows="<?php echo $arrows; ?>"  >

								<?php if(isset($image) && !empty($image) && $card_columns == "No") { ?>
									<div class="image <?php if($circle) { echo "circle"; } ?>">
										<img src="<?php echo $image['url']; ?>" alt="column image" title="Column Image" />
									</div>
								<?php } ?>
								<?php if( $card_columns == "Yes"){ echo '<div class="card-wrapper" style="background:url('.$image['url'].');background-size: cover;background-position:center;"><div class="card-content">'; } ?>
								<?php if(isset($title) && !empty($title)) { ?>
									<h2><?php echo $title; ?></h2>
								<?php } ?>
								
								<?php if( $card_columns == "Yes"){ echo '<div class="card-reveal"><div data-simplebar="init" class="card-scroll">'; } ?>

								<?php if(isset($content) && !empty($content)) { ?>
									<p><?php echo $content; ?></p>
								<?php } ?>

								<?php if( $card_columns == "Yes"){ echo '</div>'; } ?>

								<?php if(isset($card_button['title']) && $card_button['title'] != null) { ?>
									<p><a class="btn alt2" href="<?php echo $card_button['url'] ?>"><?php echo $card_button['title'] ?></a></p>
								<?php } ?>
								
								<?php if( $card_columns == "Yes"){ echo '</div>'; } ?>

								<?php if(isset($link) && $link != null) { ?>
									<a class="btn feature" href="<?php echo $link['url'] ?>"><?php echo $link['title'] ?></a>
								<?php } ?>
								<?php if( $card_columns == "Yes"){ echo '</div></div>'; } ?>
							</div>

						<?php endwhile; ?>

					<?php endif; ?>

				<?php } ?>

			</section>

		</div>

	</article>

<?php } ?>

<?php
	// Restore original Post Data
	wp_reset_postdata();
?>
