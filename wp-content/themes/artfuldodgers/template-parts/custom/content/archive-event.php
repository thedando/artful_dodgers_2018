<?php
	$img = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'large' );
	$price = get_field('dcf_price');
	$paid = get_field('dcf_paid_event');
	$date = get_field('dcf_event_date');
	$archive = get_field('dcf_archived');
	$post_class = $archive.' blogpost-entry post-item mix';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?> data-order="<?php echo $i; ?>" event-date="<?php echo $date; ?>" data-mixitupref="<?php echo strtolower(get_the_title()); ?>">

		<div class="content">
			<!-- meta -->
			<header>
				<div class="category">
					<?php
						$terms = wp_get_post_terms( get_the_ID(), 'event_cat', $args );
						print_r($terms);
						foreach($terms as $term) {
							echo '<span class="cat">';
								echo $term->name;
							echo '</span>';
						}
					?>
				</div>
				<div class="date">
					<?php if(isset($date) && !empty($date)) {
						$cur_year = date("Y");
						$ev_year = intval($date);
						if($ev_year < $cur_year) {
							$newDate = date("d M, y", strtotime($date));
						} else {
							$newDate = date("d M", strtotime($date));
						}
						echo '<span class="the_date">'.$newDate.'</span>';
					} ?>
				</div>
			</header>

			<!-- content -->
			<div class="entry-content">
				<h2><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></h2>
			</div>

			<!-- footer -->
			<footer>
				<div class="readmore">
					<a href="<?php echo get_permalink(get_the_ID()); ?>">
						<span class="readmore">
							READ MORE
						</span>
					</a>
				</div>

				<div class="price">
					<?php if($paid) { ?>
						<span class="price">
							<?php if(isset($price) && !empty($price)) { echo '$'.$price; } ?>
						</span>
					<?php } else { echo '<span class="price free">FREE</span>'; } ?>
				</div>
			</footer>

		</div>

		<span class="bgimg" style="background-image: url(<?php echo $img[0]; ?>);"></span>


</article>
