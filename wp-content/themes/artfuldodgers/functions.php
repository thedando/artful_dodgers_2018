<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );

/*** CUSTOM ADDITIONS */
require_once( 'library/custom-overrides.php' );
require_once( 'library/custom-acf.php' );
require_once( 'library/custom-functions.php' );
require_once( 'library/custom-require-plugins.php' );
require_once( 'library/custom-users.php' );
require_once( 'library/custom-menus.php' );
require_once( 'library/custom-shortcodes.php' );
require_once( 'library/custom-theme-content.php' );
require_once( 'library/custom-event-category-change.php' );

/** CUSTOM POST TYPES */
// require_once( 'library/types/CUSTOM_TYPE_ctas.php' );
require_once( 'library/types/CUSTOM_TYPE_sliders.php' );
require_once( 'library/types/CUSTOM_TYPE_events.php' );
// require_once( 'library/types/CUSTOM_TYPE_testimonials.php' );
