<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

		// SETUP: Load the Template Stylesheet FIRST !!!
		global $is_IE;
		if ( $is_IE ) {
			// Enqueue the main Stylesheet.
			wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet0', get_template_directory_uri() . '/assets/stylesheets/foundation.0.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet1', get_template_directory_uri() . '/assets/stylesheets/foundation.1.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet2', get_template_directory_uri() . '/assets/stylesheets/foundation.2.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet3', get_template_directory_uri() . '/assets/stylesheets/foundation.3.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet4', get_template_directory_uri() . '/assets/stylesheets/foundation.4.css', array(), '2.9.0', 'all' );
			wp_enqueue_style( 'main-stylesheet5', get_template_directory_uri() . '/assets/stylesheets/foundation.5.css', array(), '2.9.0', 'all' );
		} else {
			// Enqueue the main Stylesheet.
			wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.9.0', 'all' );
		}

		// Owl Slider CSS
		wp_enqueue_style( 'owl-styles', get_template_directory_uri() . '/assets/stylesheets/vendor/owl.carousel.css', array(), '', 'all' );

		// Simplebar CSS
		wp_enqueue_style( 'simplebar', get_template_directory_uri() . '/assets/stylesheets/vendor/simplebar.css', array(), '', 'all' );

		// Deregister the jquery version bundled with WordPress.
		wp_deregister_script( 'jquery' );

		// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
		wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );


		// Owl Slider JS
		wp_enqueue_script( 'mbv', get_template_directory_uri() . '/assets/javascript/vendor/owl.carousel.min.js', array('jquery'), '', false );
		wp_enqueue_script( 'mixitup', get_template_directory_uri() . '/assets/javascript/vendor/mixitup.min.js', array(), NULL, true );
		wp_enqueue_script( 'mixitup-multifilter', get_template_directory_uri() . '/assets/javascript/vendor/mixitup-multifilter.min.js', array(), NULL, true );
		wp_enqueue_script( 'mixitup_pagination', get_template_directory_uri() . '/assets/javascript/vendor/mixitup-pagination.min.js', array(), NULL, true );

		// Simplebar JS
		wp_enqueue_script( 'simplebar', get_template_directory_uri() . '/assets/javascript/vendor/simplebar.js', array('jquery'), '', false );

		// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
		// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
		wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.9.0', true );



		// Add the comment-reply library on pages where it is necessary
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// Load Custom jQuery
		wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/javascript/custom.js', array('jquery'), '', true );

		if ( class_exists('acf') && get_field('dcf_header_option') == 'video' ) {
			wp_enqueue_script( 'mbyt', get_template_directory_uri() . '/assets/javascript/vendor/jquery.mb.YTPlayer.min.js', array('jquery'), '3.0.20', false );
			wp_enqueue_style( 'mbyt-styles', get_template_directory_uri() . '/assets/stylesheets/vendor/YTPlayer/jquery.mb.YTPlayer.min.css', array(), '3.0.20', 'all' );
			wp_enqueue_script( 'mbv', get_template_directory_uri() . '/assets/javascript/vendor/jquery.mb.vimeo_player.min.js', array('jquery'), '3.0.20', false );
			wp_enqueue_style( 'mbv-styles', get_template_directory_uri() . '/assets/stylesheets/vendor/YTPlayer/jquery.mb.vimeo_player.css', array(), '3.0.20', 'all' );
		}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;
