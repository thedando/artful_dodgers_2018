<?php
/**
 * Event category change based on date
 *
 * @package foundationWP
 */

 function event_preset_category( $post_id, $post, $update ) {

     $slug = 'events'; //Slug of CPT
     // If this isn't the right slug, don't update it.
     if ( $slug != $post->post_type ) {
         return;
     }
     // Get the ID of default/ fallback category
     // $default_term = get_term_by('slug', 'your_term_slug', 'your_custom_taxonomy');
     $default_term = get_term_by('slug', 'current', 'event_archive');
     wp_set_object_terms( get_the_ID(), $default_term->term_id, 'event_archive' );
 }

 // add_action( 'save_post', 'event_preset_category', 10, 3 );





 // Create a cron job to run the day after an event happens or ends
 function set_expiry_date( $post_id ) {

   // See if an event_end_date or event_date has been entered and if not then end the function
   if( get_post_meta( $post_id, $key = 'dcf_event_date', $single = true ) ) {

     // Get the end date of the event in unix grenwich mean time
     $acf_end_date = get_post_meta( $post_id, $key = 'dcf_event_date', $single = true );

   } elseif ( get_post_meta( $post_id, $key = 'event_date', $single = true ) ) {

     // Get the start date of the event in unix grenwich mean time
     $acf_end_date = get_post_meta( $post_id, $key = 'event_date', $single = true );

   } else {

     // No start or end date. Lets delete any CRON jobs related to this post and end the function.
     wp_clear_scheduled_hook( 'make_past_event', array( $post_id ) );
     return;
   }

   // Convert our date to the correct format
   $unix_acf_end_date = strtotime( $acf_end_date );
   $gmt_end_date = gmdate( 'Ymd', $unix_acf_end_date );
   $unix_gmt_end_date = strtotime( $gmt_end_date );

   // Get the number of seconds in a day
   $delay = 24 * 60 * 60; //24 hours * 60 minutes * 60 seconds

   // Add 1 day to the end date to get the day after the event
   $day_after_event = $unix_gmt_end_date + $delay;

   // Temporarily remove from 'Past Event' category
   wp_remove_object_terms( $post_id, 'current', 'event_archive' );

   // If a CRON job exists with this post_id them remove it
   wp_clear_scheduled_hook( 'make_past_event', array( $post_id ) );
   // Add the new CRON job to run the day after the event with the post_id as an argument
   wp_schedule_single_event( $day_after_event , 'make_past_event', array( $post_id ) );

 }
 // Hook into the save_post_{post-type} function to create/update the cron job everytime an event is saved.
 // add_action( 'acf/save_post', 'set_expiry_date', 20 );



 // Create a function that adds the post to the past-events category
function set_past_event_category( $post_id ){
  // Set the post category to 'Past Event'
  wp_set_post_categories( $post_id, array( 53 ), true );
}
// Hook into the make_past_event CRON job so that the set_past_event_category function runs when the CRON job is fired.
// add_action( 'make_past_event', 'set_past_event_category' );
