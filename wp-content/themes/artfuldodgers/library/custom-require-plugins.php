<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme DANDO
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once get_template_directory() . '/library/class-tgm-plugin-activation.php';

// ONLY SHOW for users with update capabilities (i.e. Administrator)
if ( current_user_can('update_core') ) {
	add_action( 'tgmpa_register', 'DANDO_register_required_plugins' );
}

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function DANDO_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		// array(
		// 	'name'               => 'TGM Example Plugin', // The plugin name.
		// 	'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
		// 	'source'             => get_template_directory() . '/lib/plugins/tgm-example-plugin.zip', // The plugin source.
		// 	'required'           => true, // If false, the plugin is only 'recommended' instead of required.
		// 	'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
		// 	'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
		// 	'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
		// 	'external_url'       => '', // If set, overrides default API URL and points to an external URL.
		// 	'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		// ),
		array(
			'name'               => 'Advanced Custom Fields PRO', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/library/plugins/advanced-custom-fields-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '5.5.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Gravity Forms', // The plugin name.
			'slug'               => 'gravityforms', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/library/plugins/gravityforms.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '2.2.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'UpdraftPlus', // The plugin name.
			'slug'               => 'updraftplus', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/library/plugins/updraftplus.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '2.13.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'WP Migrate DB Pro', // The plugin name.
			'slug'               => 'wp-migrate-db-pro', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/library/plugins/wp-migrate-db-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.7.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'WP Migrate DB Pro Media Files', // The plugin name.
			'slug'               => 'wp-migrate-db-pro-media-files', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/library/plugins/wp-migrate-db-pro-media-files.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '1.4.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		// array(
		// 	'name'         => 'TGM New Media Plugin', // The plugin name.
		// 	'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
		// 	'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
		// 	'required'     => true, // If false, the plugin is only 'recommended' instead of required.
		// 	'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
		// ),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		// array(
		// 	'name'      => 'Adminbar Link Comments to Pending',
		// 	'slug'      => 'adminbar-link-comments-to-pending',
		// 	'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		// ),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		// array(
		// 	'name'      => 'BuddyPress',
		// 	'slug'      => 'buddypress',
		// 	'required'  => false,
		// ),

		array(
			'name' 				=> 'Advanced Access Manager',
			'slug' 				=> 'advanced-access-manager',
			'required' 			=> false,
			'force_activation'	=> false,
		),
		// array(
		// 	'name' 				=> 'Akismet',
		// 	'slug' 				=> 'akismet',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Apocalypse Meow',
		// 	'slug' 				=> 'apocalypse-meow',
		// 	'required' 			=> true,
		// 	'force_activation'	=> true
		// ),
		array(
			'name' 				=> 'Attachment Pages Redirect',
			'slug' 				=> 'attachment-pages-redirect',
			'required' 			=> false,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'Broken Link Checker',
		// 	'slug' 				=> 'broken-link-checker',
		// 	'required' 			=> true,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Check Email',
		// 	'slug' 				=> 'check-email',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Capability Manager Enhanced',
		// 	'slug' 				=> 'capability-manager-enhanced',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'Disable Comments',
			'slug' 				=> 'disable-comments',
			'required' 			=> false,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'Display widgets',
		// 	'slug' 				=> 'display-widgets',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'Duplicate Post',
			'slug' 				=> 'duplicate-post',
			'required' 			=> false,
			'force_activation'	=> false
		),
		array(
			'name' 				=> 'Google Tag Manager for Wordpress',
			'slug' 				=> 'duracelltomi-google-tag-manager',
			'required' 			=> true,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'EWWW Image Optimizer',
		// 	'slug' 				=> 'ewww-image-optimizer',
		// 	'required' 			=> true,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Image Cleanup',
		// 	'slug' 				=> 'image-cleanup',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Imsanity',
		// 	'slug' 				=> 'imsanity',
		// 	'required' 			=> true,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Media File Renamer',
		// 	'slug' 				=> 'media-file-renamer',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Members',
		// 	'slug' 				=> 'members',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Quick Featured Images',
		// 	'slug' 				=> 'quick-featured-images',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'Redirection',
			'slug' 				=> 'redirection',
			'required' 			=> false,
			'force_activation'	=> false
		),
		array(
			'name' 				=> 'Regenerate Thumbnails',
			'slug' 				=> 'regenerate-thumbnails',
			'required' 			=> false,
			'force_activation'	=> false
		),
		array(
			'name' 				=> 'Relevanssi',
			'slug' 				=> 'relevanssi',
			'required' 			=> false,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'Sidebar Login',
		// 	'slug' 				=> 'sidebar-login',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'Simple Custom Post Order',
			'slug' 				=> 'simple-custom-post-order',
			'required' 			=> false,
			'force_activation'	=> false
		),
		array(
			'name' 				=> 'SSL Insecure Content Fixer',
			'slug' 				=> 'ssl-insecure-content-fixer',
			'required' 			=> false,
			'force_activation'	=> false
		),
		array(
			'name' 				=> 'Sucuri Security - Auditing, Malware Scanner and Hardening',
			'slug' 				=> 'sucuri-scanner',
			'required' 			=> true,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'Transient Cleaner',
		// 	'slug' 				=> 'artiss-transient-cleaner',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Video Thumbnails',
		// 	'slug' 				=> 'video-thumbnails',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'Widget Options',
			'slug' 				=> 'widget-options',
			'required' 			=> false,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'WP Super Cache',
		// 	'slug' 				=> 'wp-super-cache',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'WordPress Importer',
			'slug' 				=> 'wordpress-importer',
			'required' 			=> true,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'WordPress Infinite Scroll – Ajax Load More',
		// 	'slug' 				=> 'ajax-load-more',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'WP Inline Comment Errors',
		// 	'slug' 				=> 'wp-inline-comment-errors',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'WP Mail SMTP',
			'slug' 				=> 'wp-mail-smtp',
			'required' 			=> false,
			'force_activation'	=> false
		),
		// array(
		// 	'name' 				=> 'WP-Optimize',
		// 	'slug' 				=> 'wp-optimize',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		// array(
		// 	'name' 				=> 'Wordfence',
		// 	'slug' 				=> 'wordfence',
		// 	'required' 			=> false,
		// 	'force_activation'	=> false
		// ),
		array(
			'name' 				=> 'WP Security Audit Log',
			'slug' 				=> 'wp-security-audit-log',
			'required' 			=> true,
			'force_activation'	=> false
		),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.

		// * * ACF Spesific plugins
		// * More options available with the following suggestions:
		// https://wordpress.org/plugins/advanced-custom-fields-table-field/
		// https://en-au.wordpress.org/plugins/acf-focal-point/
		// https://wordpress.org/plugins/acf-to-rest-api/
		// https://github.com/reyhoun/acf-typography
		// https://github.com/ractoon/acf-section-styles

		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Advanced Taxonomy Selector',
		// 	'slug' 					=> 'acf-advanced-taxonomy-selector',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		array(
			'name' 					=> 'Advanced Custom Fields: Gravityforms Add-on',
			'slug' 					=> 'acf-gravityforms-add-on',
			'required' 				=> true,
			'force_activation'		=> false,
			'force_deactivation' 	=> true,
			'is_callable'			=> 'acf_field',
		),
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Image Crop Add-on',
		// 	'slug' 					=> 'acf-image-crop-add-on',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Limiter',
		// 	'slug' 					=> 'advanced-custom-fields-limiter-field',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Link Picker Field',
		// 	'slug' 					=> 'acf-link-picker-field',
		// 	'required' 				=> true,
		// 	'force_activation'		=> true,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Menu Field',
		// 	'slug' 					=> 'menu-field-for-advanced-custom-fields',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		//
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Post Type Selector',
		// 	'slug' 					=> 'acf-post-type-selector',
		// 	'source' 				=> 'https://github.com/TimPerry/acf-post-type-selector/archive/master.zip',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		// array(
		// 	'name' 					=> 'Advanced Custom Fields: Widget Area',
		// 	'slug' 					=> 'acf-widget-arear',
		// 	'source' 				=> 'https://github.com/dustyf/acf-widget-area/archive/master.zip',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),
		// array(
		// 	'name' 					=> 'ACF Section Styles Field',
		// 	'slug' 					=> 'acf-section-styles',
		// 	'source' 				=> 'https://github.com/ractoon/acf-section-styles/archive/master.zip',
		// 	'required' 				=> true,
		// 	'force_activation'		=> false,
		// 	'force_deactivation' 	=> true,
		// 	'is_callable'			=> 'acf_field',
		// ),

		// * Yoast SEO Spesific plugins

		array(
			'name'				=> 'WordPress SEO by Yoast',
			'slug'				=> 'wordpress-seo',
			'required' 			=> true,
			'force_activation'	=> false,
			'is_callable'		=> 'wpseo_init',
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'DANDO',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'required-plugins',      // Menu slug.
		'parent_slug'  => 'plugins.php',           // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'DANDO' ),
			'menu_title'                      => __( 'Required Plugins', 'DANDO' ),
			'installing'                      => __( 'Installing Plugin: %s', 'DANDO' ),
			'updating'                        => __( 'Updating Plugin: %s', 'DANDO' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'DANDO' ),
			'notice_can_install_required'     => _n_noop(
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'DANDO'
			),
			'notice_can_install_recommended'  => _n_noop(
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'DANDO'
			),
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'DANDO'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'DANDO'
			),
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'DANDO'
			),
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'DANDO'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'DANDO'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'DANDO'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'DANDO'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'DANDO' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'DANDO' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'DANDO' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'DANDO' ),
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'DANDO' ),
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'DANDO' ),
			'dismiss'                         => __( 'Dismiss this notice', 'DANDO' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'DANDO' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'DANDO' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
	);

	tgmpa( $plugins, $config );
}
