# README #

Fork this repository for all new Dando projects. 

Make sure to work on the "development" branch by creating your own "development-user" branch from "development", and then merge back to "development" for the rest of the team. This branch is deployed to local and working server.

Once ready for client review, merge the "development" branch into the "staging" branch and should not be directly edited or worked on. This branch is deployed to staging server.

The "Master" branch should NOT be changed or edited. Merge "development" into "master" only when launching project or deploying to live server.